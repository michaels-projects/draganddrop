package com.michael.airbnb.draganddrop;

import android.content.ClipData;
import android.content.ClipDescription;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ImageView trashView;
    private ColorAdapter colorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        trashView = findViewById(R.id.trashView);
        recyclerView = findViewById(R.id.imagesView);

        colorAdapter = new ColorAdapter();
        colorAdapter.colors = new ArrayList<>();
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                for(int k = 0; k < 3; k++) {
                    colorAdapter.colors.add(Color.rgb(((float)1/3)*i, ((float)1/3)*j, ((float)1/3)*k));
                }
            }
        }

        recyclerView.setAdapter(colorAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));

        trashView.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent dragEvent) {
                if(dragEvent.getAction() == DragEvent.ACTION_DROP) {
                    ClipData.Item item = dragEvent.getClipData().getItemAt(0);
                    int row = Integer.valueOf(item.getText().toString());
                    Logger.getAnonymousLogger().log(Level.INFO, String.format("Entry: %d", row));
                    colorAdapter.colors.remove(row);
                    colorAdapter.notifyItemRemoved(row);
                }
                return true;
            }
        });
    }

    private static class ColorViewHolder extends RecyclerView.ViewHolder {
        int row;
        public ColorViewHolder(@NonNull View itemView, int row) {
            super(itemView);
            this.row = row;
        }
    }

    private static class ColorAdapter extends RecyclerView.Adapter<ColorViewHolder> {
        List<Integer> colors;
        @NonNull
        @Override
        public ColorViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_color, viewGroup, false);

            final ColorViewHolder holder = new ColorViewHolder(v, i);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull final ColorViewHolder viewHolder, final int i) {
            viewHolder.itemView.setBackgroundColor(colors.get(i));
            viewHolder.row = i;
            viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    int row = viewHolder.getAdapterPosition();
                    ClipData.Item item = new ClipData.Item(String.format("%s", row));
                    ClipDescription description = new ClipDescription("row", new String[] {"text/plain"});
                    ClipData data = new ClipData(description, item);
                    view.startDragAndDrop(data, new MyDragShadowBuilder(view), new Integer(row), 0);
                    return true;
                }
            });
        }

        @Override
        public int getItemCount() {
            return colors.size();
        }
    }

    private static class MyDragShadowBuilder extends View.DragShadowBuilder {

        // The drag shadow image, defined as a drawable thing
        private static Drawable shadow;

        // Defines the constructor for myDragShadowBuilder
        public MyDragShadowBuilder(View v) {

            // Stores the View parameter passed to myDragShadowBuilder.
            super(v);

            // Creates a draggable image that will fill the Canvas provided by the system.
            shadow = new ColorDrawable(Color.LTGRAY);
        }

        // Defines a callback that sends the drag shadow dimensions and touch point back to the
        // system.
        @Override
        public void onProvideShadowMetrics (Point size, Point touch) {
            // Defines local variables
            int width, height;

            // Sets the width of the shadow to half the width of the original View
            width = getView().getWidth() / 2;

            // Sets the height of the shadow to half the height of the original View
            height = getView().getHeight() / 2;

            // The drag shadow is a ColorDrawable. This sets its dimensions to be the same as the
            // Canvas that the system will provide. As a result, the drag shadow will fill the
            // Canvas.
            shadow.setBounds(0, 0, width, height);

            // Sets the size parameter's width and height values. These get back to the system
            // through the size parameter.
            size.set(width, height);

            // Sets the touch point's position to be in the middle of the drag shadow
            touch.set(width / 2, height / 2);
        }

        // Defines a callback that draws the drag shadow in a Canvas that the system constructs
        // from the dimensions passed in onProvideShadowMetrics().
        @Override
        public void onDrawShadow(Canvas canvas) {

            // Draws the ColorDrawable in the Canvas passed in from the system.
            shadow.draw(canvas);
        }
    }
}
